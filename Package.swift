// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "ButikBot",
    dependencies: [
      .package(url: "https://github.com/IBM-Swift/Kitura.git", .upToNextMinor(from: "2.4.0")),
      .package(url: "https://github.com/IBM-Swift/HeliumLogger.git", .upToNextMinor(from: "1.7.1")),
      .package(url: "https://github.com/PerfectlySoft/Perfect-PostgreSQL.git", .upToNextMinor(from: "3.1.4")),
      .package(url: "https://github.com/IBM-Swift/FileKit.git", .upToNextMinor(from: "0.0.1")),
      .package(url: "https://github.com/crossroadlabs/Regex.git", .upToNextMinor(from: "1.1.0"))
    ],
    targets: [
      .target(name: "ButikBot", dependencies: [ .target(name: "Application"), "Kitura" , "HeliumLogger" ]),
      .target(name: "Application", dependencies: [ "Kitura", "FileKit", "PerfectPostgreSQL", "Regex"]),
      .testTarget(name: "ApplicationTests" , dependencies: [ .target(name: "Application"), "Kitura", "HeliumLogger" ])
    ]
)
