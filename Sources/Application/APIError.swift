import Foundation

enum APIError: Error {
    case missingUser(String)
    case missingFace(Int)
    case missingInventorySnapshot(Int)
}

extension APIError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .missingUser(let username):
            return "API error: could not find user \(username)"
        case .missingFace(let faceId):
            return "API error: could not find face with id \(faceId)"
        case .missingInventorySnapshot(let snapshotId):
            return "API error: could not find inventory snaphot with id \(snapshotId)"
        }
    }
}

