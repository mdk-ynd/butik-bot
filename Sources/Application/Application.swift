import Foundation
import Kitura
import KituraNet
import LoggerAPI
import FileKit
import Regex

public let projectPath = FileKit.projectFolder

public class App {
    let router = Router()
    let configuration: Configuration
    let slackAPI: SlackAPI
    let database: Database

    public init() throws {
        self.configuration = try Configuration.loadFromEnvironment()
        self.slackAPI = SlackAPI(apiToken: configuration.slackAPIToken)
        self.database = Database(connectTo: configuration.databaseUrl)
    }

    internal func validate(slackRequest request: RouterRequest) throws -> [String: String] {
        // Ensure input is valid JSON
        guard let inputData = request.body?.asURLEncoded else {
            throw InputError(reason: "Invalid POST body")
        }

        // Ensure security token matches
        guard let inputToken = inputData["token"], inputToken == configuration.slackVerificationToken else {
            throw InputError(reason: "Bad slack verification token specified")
        }

        return inputData
    }

    internal func validate(apiRequest request: RouterRequest) throws -> [String: Any] {
        guard let authorizationString = request.headers["Authorization"],
            let headerToken = "^Bearer\\s+(\\w+)".r?.findFirst(in: authorizationString)?.group(at: 1) else {
                throw InputError(missingParams: ["Authorization"])
        }

        guard headerToken == self.configuration.apiToken else {
            throw AuthError(invalidApiKey: headerToken)
        }

        // Ensure input is valid JSON
        return request.body?.asJSON ?? [String: Any]()
    }

    func postInit() throws {
        router.all() { request, response, next in
            Log.info("\(request.method) \(request.urlURL)")
            next()
        }

        router.get("/") { request, response, _ in
            try response.send("ButikBot up and running.").end()
        }

        router.post("/cmd/reward", middleware: BodyParser())
        router.post("/cmd/reward") { request, response, _ in
            let inputData = try self.validate(slackRequest: request)

            // Ensure input is correct
            guard let cmdText = inputData["text"], let cmdUser = inputData["user_name"] else {
                throw InputError(missingParams: ["text", "user_name"])
            }

            let responseData = try RewardCommand(cmdText: cmdText, sender: cmdUser).perform(database: self.database)
            try response.send(json: responseData).end()
        }

        router.post("/cmd/steal", middleware: BodyParser())
        router.post("/cmd/steal") { request, response, _ in
            let inputData = try self.validate(slackRequest: request)

            // Ensure input is correct
            guard let cmdText = inputData["text"], let cmdUser = inputData["user_name"] else {
                throw InputError(missingParams: ["text", "user_name"])
            }

            let responseData = try StealCommand(cmdText: cmdText, sender: cmdUser).perform(database: self.database)
            try response.send(json: responseData).end()
        }

        router.post("/cmd/info", middleware: BodyParser())
        router.post("/cmd/info") { request, response, _ in
            let inputData = try self.validate(slackRequest: request)

            // Ensure input is correct
            guard let cmdUser = inputData["user_name"], let userId = inputData["user_id"] else {
                throw InputError(missingParams: ["user_name", "userId"])
            }

            let responseData = try InfoCommand(sender: cmdUser, userId: userId).perform(database: self.database)
            try response.send(json: responseData).end()
        }

        router.post("/cmd/transfer", middleware: BodyParser())
        router.post("/cmd/transfer") { request, response, _ in
            let inputData = try self.validate(slackRequest: request)

            // Ensure input is correct
            guard let cmdText = inputData["text"], let cmdUser = inputData["user_name"] else {
                throw InputError(missingParams: ["text", "user_name"])
            }

            let responseData = try TransferCommand(cmdText: cmdText, sender: cmdUser).perform(database: self.database)
            try response.send(json: responseData).end()
        }

        router.post("/cmd/stock", middleware: BodyParser())
        router.post("/cmd/stock") { request, response, _ in
            _ = try self.validate(slackRequest: request)
            let responseData = try StockCommand().perform(database: self.database)
            try response.send(json: responseData).end()
        }

        router.post("/cmd/history", middleware: BodyParser())
        router.post("/cmd/history") { request, response, _ in
            let inputData = try self.validate(slackRequest: request)

            // Ensure input is correct
            guard let cmdUser = inputData["user_name"], let userId = inputData["user_id"] else {
                throw InputError(missingParams: ["user_name", "userId"])
            }

            let responseData = try HistoryCommand(sender: cmdUser, userId: userId).perform(database: self.database)
            try response.send(json: responseData).end()
        }

        router.post("/cmd/stats", middleware: BodyParser())
        router.post("/cmd/stats") { request, response, _ in
            let text = """
            Everybody loves *Perrier* (3 sold in last 48h) 😁
            Nobody likes *Coke* (0 sales this week) 😕
            """

            let responseData: [String: Any] = [
                "response_type": "in_channel",
                "text": text
            ]

            try response.send(json: responseData).end()
        }

        router.get("/faces", middleware: BodyParser())
        router.get("/faces") { request, response, _ in
            _ = try self.validate(apiRequest: request)

            let faces = try self.database.getDistinctFaces()

            struct FacesResponse: Codable {
                let faces: [FaceInfoDesc]
            }

            try response.send(json: FacesResponse(faces: faces)).end()
        }

        router.get("/faces/:faceId", middleware: BodyParser())
        router.get("/faces/:faceId") { request, response, _ in
            _ = try self.validate(apiRequest: request)

            guard let faceId = Int(request.parameters["faceId"] ?? "") else {
                throw InputError(missingParams: ["faceId"])
            }

            guard let faceData = try self.database.getFaceJPEGData(faceId: faceId) else {
                throw APIError.missingFace(faceId)
            }

            response.headers["Content-Type"] = "image/jpeg"
            try response.send(data: faceData).end()
        }

        router.get("/users/:username", middleware: BodyParser())
        router.get("/users/:username") { request, response, _ in
            _ = try self.validate(apiRequest: request)

            guard let username = request.parameters["username"], username != "" else {
                throw InputError(missingParams: ["username"])
            }

            guard let user = try self.database.getUser(username: username) else {
                throw APIError.missingUser(username)
            }

            let userCredits = try self.database.getUserBalance(username: user.username)

            let responseData: [String: Any] = [
                "username": username,
                "credits": userCredits
            ]

            try response.send(json: responseData).end()
        }

        router.post("/inventory-snapshots/") { request, response, _ in
            // FIXME Duplicated from the API request validation. Rewrite to support automatically
            // the right thing when JSON data seralization simplified
            guard let authorizationString = request.headers["Authorization"],
                let headerToken = "^Bearer\\s+(\\w+)".r?.findFirst(in: authorizationString)?.group(at: 1) else {
                    throw InputError(missingParams: ["Authorization"])
            }

            guard headerToken == self.configuration.apiToken else {
                throw AuthError(invalidApiKey: headerToken)
            }

            // Here we-reparse the body as JSON to easily map it into a structure
            struct NewInventorySnapshot: Decodable {
                let items: [InventorySnapshotInfo.ProductEntry]
                let image: String?
            }

            let snapshot = try request.read(as: NewInventorySnapshot.self)
            if let base64Data = snapshot.image, let imageData = Data(base64Encoded: base64Data) {
                try self.database.saveInventorySnapshot(InventorySnapshotInfo(items: snapshot.items, image: imageData))
            } else{
                try self.database.saveInventorySnapshot(InventorySnapshotInfo(items: snapshot.items, image: nil))
            }

            let responseData: [String: Any] = [:]
            try response.send(json: responseData).end()
        }

        router.get("/inventory-snapshots/:snapshotId/image") { request, response, _ in
            guard let snapshotId = Int(request.parameters["snapshotId"] ?? "") else {
                throw InputError(missingParams: ["snapshotId"])
            }

            guard let snapshot = try self.database.getInventorySnapshot(id: snapshotId),
                  let data = snapshot.1.image else {
                throw APIError.missingInventorySnapshot(snapshotId)
            }

            response.headers["Content-Type"] = "image/jpeg"
            try response.send(data: data).end()
        }

        router.post("/users/:username/transactions", middleware: BodyParser())
        router.post("/users/:username/transactions") { request, response, _ in
            let inputData = try self.validate(apiRequest: request)

            guard let username = request.parameters["username"], username != "" else {
                throw InputError(missingParams: ["username"])
            }

            guard let amount = Int(inputData["amount"] as? String ?? "") else {
                throw InputError(reason: "Amount not specified")
            }

            guard let description = inputData["description"] as? String else {
                throw InputError(reason: "Description not specified")
            }

            guard let user = try self.database.getUser(username: username) else {
                throw APIError.missingUser(username)
            }

            let transaction = TransactionInfo(username: user.username , description: description, amount: amount)
            try self.database.saveTransaction(transaction)

            let responseData: [String: Any] = [
                "username": user.username,
                "credits": try self.database.getUserBalance(username: user.username)
            ]

            try response.send(json: responseData).end()
        }

        router.post("/bot", middleware: BodyParser())
        router.post("/bot") { request, response, _ in
            Log.debug(request.body.debugDescription)

            // Ensure input is valid JSON
            guard let inputData = request.body?.asJSON else {
                throw InputError(reason: "Invalid POST body")
            }

            // Ensure security token matches
            guard let inputToken = inputData["token"] as? String, inputToken == self.configuration.slackVerificationToken else {
                throw InputError(reason: "Bad slack verification token specified")
            }

            // Here we breach into handling challange-type stuff and generic stuff.
            // Prolly should be done a little bit better
            if let challange = inputData["challenge"] {
                let responseData: [String: Any] = [
                    "challenge": challange,
                ]

                Log.info("Responding to Slack bot challenge auth")
                try response.send(json: responseData).end()
            } else if let event = inputData["event"] as? Dictionary<String, Any> {
                if let files = (event["files"] as? Array<Dictionary<String, Any>>)?.first,
                   let imageURL = files["thumb_480"] as? String,
                   let user = event["user"] as? String {

                    let userName = try self.slackAPI.getUsername(forSlackUser: user)
                    let jpegData = try self.slackAPI.downloadImage(urlString: imageURL)
                    Log.info("\(userName) uploaded new thumbnail image with URL: \(imageURL)")

                    let face = FaceInfo(username: userName, jpegData: jpegData, size: jpegData.count)
                    try self.database.saveFace(face)
                } else {
                    Log.info("Unknown message from bot. Ignoring.")
                }
                try response.send("OK").end()
            }
        }

        router.error { request, response, next in
            if let slackError = response.error as? SlackCmdError {
                try response.send(json: slackError.responseData).end()
            } else if let authError = response.error as? AuthError {
                response.statusCode = .unauthorized
                Log.error("\(authError.localizedDescription)")
                try response.send(authError.localizedDescription).end()
            } else if let errorMsg = response.error {
                response.statusCode = .badRequest
                Log.error("\(errorMsg.localizedDescription)")
                try response.send(errorMsg.localizedDescription).end()
            }
        }
    }

    public func run() throws {
        let port = ProcessInfo.processInfo.environment["PORT"] ?? "8080"
        try postInit()
        Kitura.addHTTPServer(onPort: port.int!, with: router)
        Kitura.run()
    }
}
