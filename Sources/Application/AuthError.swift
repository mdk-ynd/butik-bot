import Foundation

public struct AuthError: Error {
    let message: String

    init(invalidApiKey: String) {
        message = "Auth error: \(invalidApiKey) is not a valid API key"
    }
}

extension AuthError: LocalizedError {
    public var errorDescription: String? {
        return message
    }
}
