import Foundation
import LoggerAPI

struct Configuration {
    let slackAPIToken: String
    let slackVerificationToken: String
    let apiToken: String
    let databaseUrl: String

    static func loadFromEnvironment() throws  -> Configuration {
        guard let apiToken = ProcessInfo.processInfo.environment["API_TOKEN"] else {
            throw ConfigurationError(missingVariable: "API_TOKEN")
        }

        guard let databaseUrl = ProcessInfo.processInfo.environment["DATABASE_URL"] else {
            throw ConfigurationError(missingVariable: "DATABASE_URL")
        }

        guard let slackAPIToken = ProcessInfo.processInfo.environment["SLACK_API_TOKEN"] else {
            throw ConfigurationError(missingVariable: "SLACK_API_TOKEN")
        }

        guard let slackVerificationToken = ProcessInfo.processInfo.environment["SLACK_VERIFICATION_TOKEN"] else {
            throw ConfigurationError(missingVariable: "SLACK_VERIFICATION_TOKEN")
        }

        return Configuration(slackAPIToken: slackAPIToken, slackVerificationToken: slackVerificationToken,
                             apiToken: apiToken, databaseUrl: databaseUrl)
    }
}
