import Foundation

public struct ConfigurationError: Error {
    let message: String
    init(missingVariable: String) {
        message = "Configuration error: missing environment variable: \(missingVariable)"
    }
}

extension ConfigurationError: LocalizedError {
    public var errorDescription: String? {
        return message
    }
}
