import Foundation
import PerfectPostgreSQL
import LoggerAPI

public class Database {

    public typealias TransactionCallback = () throws -> ()

    struct Error: Swift.Error {
        let message: String
        init(reason: String) {
            message = "Database error: \(reason)"
        }
    }

    var isOk: Bool {
        return pg.status() == .ok
    }

    let connectionUrl: String
    var pg = PGConnection()
    let semaphore = DispatchSemaphore(value: 1)

    public init(connectTo url: String) {
        let _ = pg.connectdb(url)
        connectionUrl = url
    }

    internal func ensureConnection() throws {
        if !self.isOk {
            Log.info("Database lost connection, reconnecting...")
            self.pg = PGConnection()
            _ = pg.connectdb(connectionUrl)
        }

        guard self.isOk else {
            throw Error(reason: "failed to re-establish database connection")
        }
    }

    func getUser(username: String) throws -> UserInfo? {
        semaphore.wait()
        defer {
            semaphore.signal()
        }

        try ensureConnection()

        let res = self.pg.exec(statement: "SELECT username, type FROM users WHERE username = $1", params: [username])

        if res.numTuples() < 1 {
            return nil
        } else {
            let username = res.getFieldString(tupleIndex: 0, fieldIndex: 0) ?? ""
            let type = res.getFieldString(tupleIndex: 0, fieldIndex: 1) ?? ""
            return UserInfo(username: username, type: type)
        }
    }

    func getFaceJPEGData(faceId: Int) throws -> Data? {
        semaphore.wait()
        defer {
            semaphore.signal()
        }
        try ensureConnection()

        let res = self.pg.exec(statement: "SELECT jpeg_data FROM faces WHERE id = $1", params: [faceId])

        if res.numTuples() < 1 {
            return nil
        } else {
            if let bytes = res.getFieldBlobUInt8(tupleIndex: 0, fieldIndex: 0) {
                return Data(bytes)
            } else {
                return nil
            }
        }
    }

    func getDistinctFaces() throws -> [FaceInfoDesc] {
        semaphore.wait()
        defer {
            semaphore.signal()
        }
        try ensureConnection()

        var arr = [FaceInfoDesc]()
        let res = self.pg.exec(statement: "SELECT DISTINCT ON(username) username, id, size FROM faces ORDER BY username, created_at DESC")

        if (res.status() != .tuplesOK && res.status() != .singleTuple) {
            throw Error(reason: "failed to get faces: \(res.errorMessage())")
        }

        for i in 0..<res.numTuples() {
            let username = res.getFieldString(tupleIndex: i, fieldIndex: 0)!
            let id = res.getFieldInt(tupleIndex: i, fieldIndex: 1)!
            let size = res.getFieldInt(tupleIndex: i, fieldIndex: 2)!
            arr.append(FaceInfoDesc(id: id, username: username, size: size))
        }

        return arr
    }

    func getUserBalance(username: String) throws -> Int {
        semaphore.wait()
        defer {
            semaphore.signal()
        }
        try ensureConnection()

        let res = self.pg.exec(statement: "SELECT SUM(amount) from transactions where transactions.username = $1", params: [username])
        if res.numTuples() < 1 {
            return 0
        } else {
            return res.getFieldInt(tupleIndex: 0, fieldIndex: 0) ?? 0
        }
    }

    func hasFaceForUser(username: String) throws -> Bool {
        semaphore.wait()
        defer {
            semaphore.signal()
        }
        try ensureConnection()

        let res = self.pg.exec(statement: "SELECT COUNT(*) FROM faces where faces.username = $1", params: [username])
        
        if res.numTuples() < 1 {
            return false
        } else {
            return res.getFieldInt(tupleIndex: 0, fieldIndex: 0)! > 0
        }
    }

    func getTransactions(forUsername username: String) throws -> [TransactionInfo] {
        semaphore.wait()
        defer {
            semaphore.signal()
        }
        try ensureConnection()

        let res = self.pg.exec(statement: "SELECT username, description, amount FROM transactions WHERE username = $1 ORDER BY created_at DESC LIMIT 32", params: [username])
        var transactions = [TransactionInfo]()

        for i in 0..<res.numTuples() {
            let username = res.getFieldString(tupleIndex: i, fieldIndex: 0)!
            let description = res.getFieldString(tupleIndex: i, fieldIndex: 1)!
            let amount = res.getFieldInt(tupleIndex: i, fieldIndex: 2)!
            transactions.append(TransactionInfo(username: username, description: description, amount: amount))
        }

        return transactions
    }

    func getLatestInventorySnapshot() throws -> (Int, InventorySnapshotInfo)? {
        semaphore.wait()
        defer {
            semaphore.signal()
        }
        try ensureConnection()

        let res = self.pg.exec(statement: "SELECT id, items, image FROM inventory_snapshots ORDER BY created_at DESC LIMIT 1 ")
        if res.numTuples() < 1 {
            return nil
        } else {
            let id = res.getFieldInt(tupleIndex: 0, fieldIndex: 0) ?? -1
            let itemsJSON = res.getFieldString(tupleIndex: 0, fieldIndex: 1) ?? "[]"
            let items = try JSONDecoder().decode([InventorySnapshotInfo.ProductEntry].self, from: itemsJSON.data(using: .utf8, allowLossyConversion: false) ?? Data())

            if let bytes = res.getFieldBlobUInt8(tupleIndex: 0, fieldIndex: 2) {
                return (id, InventorySnapshotInfo(items: items, image: Data(bytes)))
            } else {
                return (id, InventorySnapshotInfo(items: items, image: nil))
            }
        }
    }

    func saveInventorySnapshot(_ snapshot: InventorySnapshotInfo) throws {
        semaphore.wait()
        defer {
            semaphore.signal()
        }
        try ensureConnection()

        let itemsJSON = try JSONEncoder().encode(snapshot.items)
        if let imageData = snapshot.image {
            let result = self.pg.exec(statement: "INSERT INTO inventory_snapshots(items, image) VALUES($1, $2)",
                                      params: [String(data: itemsJSON, encoding: .utf8), imageData])
            if (result.status() != .commandOK) {
                throw Error(reason: "failed to save new inventory snapshot: \(result.errorMessage())")
            }
        } else {
            let result = self.pg.exec(statement: "INSERT INTO inventory_snapshots(items) VALUES($1)",
                                      params: [String(data: itemsJSON, encoding: .utf8)])
            if (result.status() != .commandOK) {
                throw Error(reason: "failed to save new inventory snapshot: \(result.errorMessage())")
            }
        }
    }

    func getInventorySnapshot(id: Int) throws -> (Int, InventorySnapshotInfo)? {
        semaphore.wait()
        defer {
            semaphore.signal()
        }
        try ensureConnection()

        let res = self.pg.exec(statement: "SELECT id, items, image FROM inventory_snapshots WHERE id = $1", params: [id])
        if res.numTuples() < 1 {
            return nil
        } else {
            let id = res.getFieldInt(tupleIndex: 0, fieldIndex: 0) ?? -1
            let itemsJSON = res.getFieldString(tupleIndex: 0, fieldIndex: 1) ?? "[]"
            let items = try JSONDecoder().decode([InventorySnapshotInfo.ProductEntry].self, from: itemsJSON.data(using: .utf8, allowLossyConversion: false) ?? Data())

            if let bytes = res.getFieldBlobUInt8(tupleIndex: 0, fieldIndex: 2) {
                return (id, InventorySnapshotInfo(items: items, image: Data(bytes)))
            } else {
                return (id, InventorySnapshotInfo(items: items, image: nil))
            }
        }
    }

    func getOrCreateUser(username: String) throws -> UserInfo {
        semaphore.wait()
        defer {
            semaphore.signal()
        }
        try ensureConnection()

        let res = self.pg.exec(statement: "SELECT username, type FROM users WHERE username = $1", params: [username])

        if res.numTuples() < 1 {
            let _ = self.pg.exec(statement: "INSERT INTO users(username) VALUES($1)", params: [username])
            return UserInfo(username: username, type: "user")
        } else {
            let username = res.getFieldString(tupleIndex: 0, fieldIndex: 0) ?? ""
            let type = res.getFieldString(tupleIndex: 0, fieldIndex: 1) ?? ""
            return UserInfo(username: username, type: type)
        }
    }

    func saveTransaction(_ t: TransactionInfo) throws {
        semaphore.wait()
        defer {
            semaphore.signal()
        }
        try ensureConnection()

        let result = self.pg.exec(statement: "INSERT INTO transactions(username, description, amount) VALUES($1, $2, $3)",
                                  params: [t.username, t.description, t.amount])

        if (result.status() != .commandOK) {
            throw Error(reason: "failed to save new transaction: \(result.errorMessage())")
        }
    }

    func saveFace(_ f: FaceInfo) throws {
        semaphore.wait()
        defer {
            semaphore.signal()
        }
        try ensureConnection()

        let result = self.pg.exec(statement: "INSERT INTO faces(username, size, jpeg_data) VALUES($1, $2, $3)",
                                  params: [f.username, f.size, f.jpegData])

        if (result.status() != .commandOK) {
            throw Error(reason: "failed to save new face: \(result.errorMessage())")
        }
    }

    func performTransaction(_ callback: TransactionCallback) throws {
        guard self.pg.exec(statement: "START TRANSACTION").status() == .commandOK else {
            throw Error(reason: "failed to start database transaction")
        }

        do {
            try callback()
        } catch {
            if self.pg.exec(statement: "ROLLBACK").status() != .commandOK {
                // This is a bit of a hardcore case. We failed to ROLLBACK.
                // Make sure we close the connection to avoid any potential damage
                pg.close()
            }

            throw error
        }

        guard self.pg.exec(statement: "COMMIT").status() == .commandOK else {
            throw Error(reason: "failed to commit database transaction")
        }
    }

    deinit {
        pg.close()
    }
}

extension Database.Error: LocalizedError {
    public var errorDescription: String? {
        return message
    }
}

