import Foundation

struct FaceInfo {
    let username: String
    let jpegData: Data
    let size: Int
}
