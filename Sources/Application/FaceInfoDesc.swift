import Foundation

struct FaceInfoDesc: Codable {
    let id: Int
    let username: String
    let size: Int
}
