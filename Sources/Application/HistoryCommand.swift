import Foundation
import LoggerAPI

public class HistoryCommand {
    let sender: String
    let userId: String

    init(sender: String, userId: String) {
        self.sender = sender
        self.userId = userId
    }

    var senderEntity: String {
        return "<@\(userId)|\(sender)>"
    }

    func perform(database: Database) throws -> [String: Any] {

        func formatAmount(_ amount: Int) -> String {
            return (amount > 0) ? "+\(amount)" : "\(amount)"
        }

        Log.info("@\(self.sender) is asking for history")

        let senderUser = try database.getOrCreateUser(username: self.sender)
        let transactions = try database.getTransactions(forUsername: senderUser.username)
        let senderCredits = try database.getUserBalance(username: senderUser.username)

        let balanceText = "BALANCE".padding(toLength: 64, withPad: " ", startingAt: 0) + formatAmount(senderCredits)
        let dividerLine = "".padding(toLength: balanceText.count, withPad: "-", startingAt: 0)

        var text = "Here is your recent account history:\n\n```\(balanceText)\n\(dividerLine)\n"

        for transaction in transactions {
            let paddedDescription = transaction.description.padding(toLength: 30, withPad: " ", startingAt: 0)
            let amountString = (transaction.amount > 0) ? "+\(transaction.amount)" : "\(transaction.amount)"
            text += "\(paddedDescription)\(amountString)\n"
        }
        text += "```"

        let responseData: [String: Any] = [
            "text": text
        ]
        return responseData
    }
}
