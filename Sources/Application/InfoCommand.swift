import Foundation
import LoggerAPI

public class InfoCommand {
    let sender: String
    let userId: String

    init(sender: String, userId: String) {
        self.sender = sender
        self.userId = userId
    }

    var senderEntity: String {
        return "<@\(userId)|\(sender)>"
    }

    func perform(database: Database) throws -> [String: Any] {
        Log.info("@\(self.sender) is asking for info")

        let senderUser = try database.getOrCreateUser(username: self.sender)
        let senderCredits = try database.getUserBalance(username: senderUser.username)

        var text = "Hello \(self.senderEntity)!"

        var creditsString = ""
        for _ in 0..<senderCredits {
            creditsString += "⭐️"
        }
        text += "\nCredit: \(creditsString)(\(senderCredits))"

        if senderUser.isAdmin {
            text += "\nYou're an *admin* user and you can give rewards to others."
        }

        if try !database.hasFaceForUser(username: senderUser.username) {
            text += "\nYou have no face uploaded so you can't shop 😒"
        } else {
            text += "\nYou have face uploaded to Butik and you can shop 👌"
        }

        let responseData: [String: Any] = [
            "text": text
        ]
        return responseData
    }
}
