import Foundation

public struct InputError: Error {
    let message: String
    init(reason: String) {
        message = "Input error: \(reason)"
    }

    init(missingParams: [String]) {
        message = "Input error, missing parameters: \(missingParams.description)"
    }

    init(badCommand: String) {
        message = "Input error, unrecognized command: \(badCommand)"
    }

    init(extracting val: String, from data: String) {
        message = "Input error, could not extract \(val) from \(data)"
    }
}

extension InputError: LocalizedError {
    public var errorDescription: String? {
        return message
    }
}
