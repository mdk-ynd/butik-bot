import Foundation

struct InventorySnapshotInfo: Codable {

    struct ProductEntry: Codable {
        let name: String
        let stock: Int
    }

    let items: [ProductEntry]
    let image: Data?
}
