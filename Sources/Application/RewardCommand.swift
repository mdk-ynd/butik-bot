import Foundation
import LoggerAPI

public class RewardCommand {
    let cmdText: String
    let sender: String

    init(cmdText: String, sender: String) {
        self.cmdText = cmdText.trimmingCharacters(in: .whitespaces)
        self.sender = sender
    }

    func perform(database: Database) throws -> [String: Any] {
        guard let usernameText = self.cmdText.components(separatedBy: " ").first else {
            throw InputError(reason: "missing person to reward!")
        }

        guard let rewardTarget = SlackAPI.extractUsername(from: usernameText) else {
            throw InputError(extracting: "username", from: usernameText)
        }

        Log.info("@\(sender) is rewarding @\(rewardTarget)")

        // You can't reward yourself
        guard self.sender != rewardTarget else {
            throw SlackCmdError(message: "Nice try! 🤥")
        }

        // Ensure user has proper access credentials
        let senderUser = try database.getOrCreateUser(username: self.sender)
        guard senderUser.isAdmin else {
            throw SlackCmdError(message: "Whoops, it doesn't look like you have required credentials to give rewards!")
        }

        try database.performTransaction {
            let description = "Reward from \(senderUser.username)"
            let receiverUser = try database.getOrCreateUser(username: rewardTarget)
            let transactionInfo = TransactionInfo(username: receiverUser.username, description: description, amount: 1)
            try database.saveTransaction(transactionInfo)
        }

        let finalCredits = try database.getUserBalance(username: rewardTarget)
        
        let text = """
        Great job \(usernameText)! You have been rewarded with one ⭐️ (you now have \(finalCredits) in total)
        Spend your credit at the Butik shop.
        """

        let responseData: [String: Any] = [
            "response_type": "in_channel",
            "text": text
        ]
        return responseData
    }
}
