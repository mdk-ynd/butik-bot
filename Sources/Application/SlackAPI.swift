import Foundation
import LoggerAPI

class SlackAPI {

    struct Error: Swift.Error {
        let message: String
        init(reason: String) {
            message = "Slack API error: \(reason)"
        }

        init(failedToFetch entity: String, withId id: String) {
            message = "Slack API error: failed to fetch \(entity) \(id)"
        }
    }

    let apiToken: String

    init(apiToken: String) {
        self.apiToken = apiToken
    }

    func downloadImage(urlString: String) throws -> Data{
        guard let url = URL(string: urlString) else {
            throw Error(reason: "Invalid URL: \(urlString)")
        }

        Log.debug("Downloading \(urlString) from Slack")

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("Bearer \(self.apiToken)", forHTTPHeaderField: "Authorization")

        var imageData: Data? = nil
        let semaphore = DispatchSemaphore(value: 0)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            let httpResponse = response as! HTTPURLResponse

            if let data = data, httpResponse.statusCode == 200 {
                Log.debug("Downloaded \(urlString): \(data.count) bytes")
                imageData = data
            } else {
                Log.warning("Failed to download \(urlString)")
            }

            semaphore.signal()
        }.resume()
        _ = semaphore.wait(timeout: DispatchTime.distantFuture)

        guard imageData != nil else {
            throw Error(failedToFetch: "image", withId: urlString)
        }

        return imageData!
    }

    func getUserDetails(_ user: String) throws -> Dictionary<String, Any> {
        guard let url = URL(string: "https://slack.com/api/users.info?user=\(user)") else {
            throw Error(reason: "invalid Slack user name, can't form URL: \(user)")
        }

        Log.debug("Getting Slack details for user \(user)")

        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("Bearer \(self.apiToken)", forHTTPHeaderField: "Authorization")

        let semaphore = DispatchSemaphore(value: 0)

        var userData: Dictionary<String, Any>? = nil
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            do {
                if let data = data {
                    userData = try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, Any>
                } else {
                    Log.error("Failed to deserialize Slack reply")
                }
            } catch {
                Log.error("Failed to call Slack API")
            }
            semaphore.signal()
        }.resume()

        _ = semaphore.wait(timeout: DispatchTime.distantFuture)

        guard userData != nil else {
            throw Error(failedToFetch: "user", withId: user)
        }

        return userData!
    }

    func getUsername(forSlackUser user: String) throws  -> String {
        let response = try self.getUserDetails(user)
        guard let userData = response["user"] as? Dictionary<String, Any>,
              let username = userData["name"] as? String else {
            throw Error(reason: "data from Slack missing user['name'] key")
        }

        return username
    }

    static internal func extractUsername(from entity: String) -> String? {
        if entity.hasPrefix("<"), entity.contains("|") {
            return entity.components(separatedBy: "|").last?.replacingOccurrences(of: ">", with: "")
        } else {
            return entity.replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "").replacingOccurrences(of: "@", with: "")
        }
    }
}
