import Foundation

public struct SlackCmdError: Error {
    let message: String

    public var responseData: [String: Any] {
        return [
            "response_type": "in_channel",
            "text": message
        ]
    }
}
