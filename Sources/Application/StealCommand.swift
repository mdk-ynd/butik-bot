import Foundation
import LoggerAPI

public class StealCommand {
    let cmdText: String
    let sender: String

    init(cmdText: String, sender: String) {
        self.cmdText = cmdText.trimmingCharacters(in: .whitespaces)
        self.sender = sender
    }

    func perform(database: Database) throws -> [String: Any] {
        guard let usernameText = self.cmdText.components(separatedBy: " ").first else {
            throw InputError(reason: "missing person to steal from!")
        }

        guard let stealTarget = SlackAPI.extractUsername(from: usernameText) else {
            throw InputError(extracting: "username", from: usernameText)
        }

        // You can't reward yourself
        guard self.sender != stealTarget else {
            throw SlackCmdError(message: "Nice try! 🤥")
        }

        // Ensure user has proper access credentials
        let senderUser = try database.getOrCreateUser(username: self.sender)
        guard senderUser.isAdmin else {
            throw SlackCmdError(message: "Whoops, it doesn't look like you have required credentials to steal credits!")
        }

        try database.performTransaction {
            let description = "Steal from \(senderUser.username)"
            let receiverUser = try database.getOrCreateUser(username: stealTarget)
            let transactionInfo = TransactionInfo(username: receiverUser.username, description: description, amount: -1)
            try database.saveTransaction(transactionInfo)
        }

        let finalCredits = try database.getUserBalance(username: stealTarget)

        let text = """
        Sorry \(usernameText), you're not doing enough! We're taking away one ⭐️ from you. (You now have \(finalCredits) in total)
        Spend your credit at the Butik shop.
        """

        let responseData: [String: Any] = [
            "response_type": "in_channel",
            "text": text
        ]
        return responseData
    }
}
