import Foundation

class StockCommand {
    func perform(database: Database) throws -> [String: Any] {

        var text = "Today I have in stock:\n"
        guard let stockEntry = try database.getLatestInventorySnapshot() else {
            return [
                "response_type": "in_channel",
                "text": "No inventory data available."
            ]
        }

        for item in stockEntry.1.items {
            text = text + "\(item.name) × *\(item.stock)*\n"
        }

        // FIXME Replace that with dynamic URL generation here
        let imageAttachment: [String: Any] = [
            "fallback": "Current product view",
            "image_url": "http://butik-bot.herokuapp.com/inventory-snapshots/\(stockEntry.0)/image",
            "text": "Current shelf view"
        ]

        let responseData: [String: Any] = [
            "response_type": "in_channel",
            "text": text,
            "attachments": [imageAttachment]
        ]

        return responseData
    }
}
