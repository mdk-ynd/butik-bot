import Foundation

struct TransactionInfo {
    let username: String
    let description: String
    let amount: Int
}
