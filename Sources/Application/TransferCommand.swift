import Foundation
import LoggerAPI

public class TransferCommand {
    let cmdText: String
    let sender: String

    init(cmdText: String, sender: String) {
        self.cmdText = cmdText.trimmingCharacters(in: .whitespaces)
        self.sender = sender
    }

    func perform(database: Database) throws -> [String: Any] {
        guard let usernameText = self.cmdText.components(separatedBy: " ").first else {
            throw InputError(reason: "missing person to transfer to!")
        }

        guard let transferTarget = SlackAPI.extractUsername(from: usernameText) else {
            throw InputError(extracting: "username", from: usernameText)
        }

        Log.info("@\(sender) is transfering to @\(transferTarget)")

        // You can't transfer to yourself
        guard self.sender != transferTarget else {
            throw SlackCmdError(message: "You can't transfer to youself! 🤥")
        }

        try database.performTransaction {
            let senderUser = try database.getOrCreateUser(username: self.sender)
            let senderCredits = try database.getUserBalance(username: senderUser.username)

            // Ensure funds are present
            guard senderCredits > 0 else {
                throw SlackCmdError(message: "Oooops! You don't have any ⭐️ available! Go back to work to earn some!")
            }

            let transferUser = try database.getOrCreateUser(username: transferTarget)

            let senderTransaction = TransactionInfo(username: senderUser.username,
                                                    description: "Transfer to \(transferUser.username)",
                                                    amount: -1)
            try database.saveTransaction(senderTransaction)

            let transferTransaction = TransactionInfo(username: transferUser.username,
                                                      description: "Transfer from \(senderUser.username)",
                                                      amount: 1)
            try database.saveTransaction(transferTransaction)
        }

        let finalCredits = try database.getUserBalance(username: transferTarget)

        let text = """
        Great news \(transferTarget)! @\(sender) transfered you one of his hard-earned ⭐️ (you now have \(finalCredits) in total)
        Spend your credit at the Butik shop.
        """

        return [
            "response_type": "in_channel",
            "text": text
        ]
    }
}
