import Foundation

struct UserInfo {
    let username: String
    let type: String

    var isAdmin: Bool {
        return type == "admin"
    }
}
