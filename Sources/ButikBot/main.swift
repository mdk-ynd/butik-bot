import Foundation
import Kitura
import LoggerAPI
import HeliumLogger
import Application

do {
    if let _ = ProcessInfo.processInfo.environment["DEBUG"] {
        HeliumLogger.use(LoggerMessageType.debug)
    } else {
        HeliumLogger.use(LoggerMessageType.info)
    }

    let app = try App()
    try app.run()

} catch let error {
    Log.error(error.localizedDescription)
}
