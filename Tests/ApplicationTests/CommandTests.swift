//
//  CommandTests.swift
//  ApplicationTests
//
//  Created by Michael Dominic K. on 03/05/2018.
//

import XCTest

class CommandTests: XCTestCase {

    static var allTests : [(String, (CommandTests) -> () throws -> Void)] {
        return [
            ("testUsernameExtraction", testUsernameExtraction)
        ]
    }

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testUsernameExtraction() {
        let username1 = RewardCommand.extractUsername(from: "<TU12345678|klembke>")
        XCTAssertEqual(username1, "klembke")
        let username2 = RewardCommand.extractUsername(from: "klembke")
        XCTAssertEqual(username2, "klembke")
        let username3 = RewardCommand.extractUsername(from: "@klembke")
        XCTAssertEqual(username3, "klembke")
        let username4 = RewardCommand.extractUsername(from: "<klembke>")
        XCTAssertEqual(username4, "klembke")
    }
}
