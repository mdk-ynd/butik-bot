//
//  DatabaseTests.swift
//  ApplicationTests
//
//  Created by Michael Dominic K. on 01/05/2018.
//

import XCTest

class DatabaseTests: XCTestCase {

    var connectUrl: String!

    static var allTests : [(String, (DatabaseTests) -> () throws -> Void)] {
        return [
            ("testConnection", testConnection),
            ("testUserCreation", testUserCreation),
            ("testUserSave", testUserSave)
        ]
    }

    override func setUp() {
        super.setUp()
        self.connectUrl = ProcessInfo.processInfo.environment["DATABASE_URL"]
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testConnection() {
        let database = Database(connectTo: self.connectUrl)
        XCTAssert(database.isOk)
    }

    func testUserCreation() {
        let database = Database(connectTo: self.connectUrl)
        let user = try! database.getOrCreateUser(username: "mdk")
        XCTAssert(user.username == "mdk")
        XCTAssert(user.type == "user")
    }

    func testUserSave() throws {
        let database = Database(connectTo: self.connectUrl)
        var user = try! database.getOrCreateUser(username: "mdk")
        user.type = "admin"
        try database.saveUser(user)
        user = try database.getOrCreateUser(username: "mdk")
        XCTAssert(user.username == "mdk")
        XCTAssert(user.type == "admin")
    }

    func testTransaction() throws {
        let database = Database(connectTo: self.connectUrl)

        var user1 = try database.getOrCreateUser(username: "mdk")
        let initialCredits1 = user1.credits
        var user2 = try database.getOrCreateUser(username: "pprobola")
        let initialCredits2 = user2.credits

        try database.performTransaction {
            user1.credits -= 1
            user2.credits += 1
            try database.saveUser(user1)
            try database.saveUser(user2)
        }

        user1 = try database.getOrCreateUser(username: "mdk")
        let finalCredits1 = user1.credits
        user2 = try database.getOrCreateUser(username: "pprobola")
        let finalCredits2 = user2.credits

        XCTAssertEqual(initialCredits1 - 1, finalCredits1)
        XCTAssertEqual(initialCredits2 + 1, finalCredits2)

        try? database.performTransaction {
            user1.credits -= 1
            try database.saveUser(user1)
            throw InputError(reason: "An error to force transaction rollback")
        }

        user1 = try! database.getOrCreateUser(username: "mdk")
        XCTAssertEqual(user1.credits, initialCredits1 - 1)
    }
}
